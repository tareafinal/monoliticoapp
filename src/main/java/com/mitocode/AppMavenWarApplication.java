package com.mitocode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppMavenWarApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppMavenWarApplication.class, args);
	}

}

