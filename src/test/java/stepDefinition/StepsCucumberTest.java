package stepDefinition;



import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Ignore;

import com.mitocode.util.Consumo;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;

///COVERAGE:OFF
public class StepsCucumberTest {
	
	private final static Logger log = Logger.getLogger(StepsCucumberTest.class);
	
	private int start;	
	private Consumo test;
	
	@Given("Hay {int} servicios")
	public void hay_servicios(Integer start) {
	    this.start = start;
	    
	}
	

	@When("Tomo {int} servicios")
	public void tomo_servicios(Integer consumidos) {
		test.ejecutados(this.start, consumidos);
	    
	    
	}

	@When("Consumo {int} servicios")
	public void consumo_servicios(Integer consumo) {
	    log.info("Consumo:"+consumo);
	}

	@Then("Deberian quedar {int} servicios")
	public void deberian_quedar_servicios(Integer consumidos) {
	    log.info("Deberian quedar:"+consumidos);
	}


	@Given("Un blog llamado {string} 	 con el siguiente contenido")
	public void un_blog_llamado_con_el_siguiente_contenido(String comillasDobles, String docString) {
		log.info(comillasDobles);
		log.info(docString);
		
	    
	}
	
	@Given("Los siguientes usuarios existentes:")
	public void los_siguientes_usuarios_existentes(DataTable dataTable) {
		List<List<String>> rows = dataTable.asList(String.class);
		//inicio es inclusivo
		//fin es exlusivo por eso no restamos -1
		List<List<String>> rowsWithoutHeader = rows.subList(1,rows.size());
		log.info("dataTAble: \n"+dataTable);

	}
	


}
