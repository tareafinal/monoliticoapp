package com.mitocode;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mitocode.util.Consumo;

import runSteps.RunCucumberTest;

public class ConsumoTest {
	
	private final static Logger log = Logger.getLogger(ConsumoTest.class);
	private Consumo consumo;
	private MessageException message;
	private RunCucumberTest cucumberRun;
	
	@Before
	public void before() {
		consumo = null;
		message = null;
		cucumberRun = null;
	}

	@Test
	public void RunCucumberTest() {
		cucumberRun = new RunCucumberTest();
		assertTrue(true);
	}
	
	@Test
	public void ejecutados() throws MessageException {
		int inicio=10;
		int consumen=5;
		consumo = new Consumo(inicio);
		int restan=consumo.ejecutados(inicio,consumen);
		log.info("Se han consumido:"+restan);
		assertTrue(true);
		
	}
	
//	@Test(expected = NullPointerException.class)
//	public void restantes() throws MessageException{
//		log.info("Test: Deberian quedar");
//		int start=10;
//		int consumidos=5;
//
//		 assertEquals(true, consumo.restantes(consumidos));
//	}
	
	
	
	
	@Test
	public void MessageException() {
		message = new MessageException("nuevo");
		assertTrue(true);
		
	}
	
	@After
	public void after() {
		consumo = null;
		message = null;
		cucumberRun = null;
		log.info("Ends\n");
	}
	
	
}
