package runSteps;

import static cucumber.api.SnippetType.CAMELCASE;

import cucumber.api.CucumberOptions;

@CucumberOptions(
		
	    features = "src/test/resources/features",
	    glue = "stepDefinitions",
	    plugin = {"pretty","json:target/cucumber-reports/cucumber.jsons"},
	    snippets = CAMELCASE
	)
public class RunCucumberTest {

}
