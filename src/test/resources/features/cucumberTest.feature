Feature: Prueba cucumber

	


  Scenario Outline: Consumiento
  	Given Hay <inicio> servicios
  	When Consumo <cantidad> servicios
  	Then Deberian quedar <resto> servicios
  	
  	Examples:
  	
  	| inicio | cantidad | resto |   
  	| 10		 |  5				|  5		|  
  	| 10		 |  10			|  0		|  
  	
  	
 Scenario: Doc String
 
 	Given Un blog llamado "BlogPrueba" 	 con el siguiente contenido
 	
 	"""
 		Esto es un texto grande de prueba
 		===================================
 		aqui cabe mucho texto
 	"""
  	
  	
Scenario: Data table
	Given Los siguientes usuarios existentes:
		| nombre | email 							| twitter |
		| yussel | yussel@correo.com 	| @yussel |
		| misha  | misha@correo.com 	| @misha  |
	  	
  	
